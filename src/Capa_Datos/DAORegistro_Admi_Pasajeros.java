/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Datos;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author yasni
 */
public class DAORegistro_Admi_Pasajeros {
    private static Scanner sc;
    private static int Intentos;
    private static String user, pwd;
    RandomAccessFile users;
    //constructor de la clase
    public DAORegistro_Admi_Pasajeros(){
         try {
            //nombre del archivo
            // r
            //rw = read and write
             users = new RandomAccessFile("usuarios.usr", "rw");
        } catch(IOException e){}
       
    }
     public long getFinal() throws  IOException{
        if (users.length()!= 0) {
            users.seek(0);
            while (users.getFilePointer() < users.length()) {                
            users.readUTF();//para leer string
            users.readUTF();
            users.readUTF();
            users.readUTF();
            users.readUTF();
            }
            
        }
        return users.getFilePointer();
    }
    
public void Write(String ususario,String contraseña) throws IOException{
    users.seek(getFinal());
    users.writeUTF(ususario);
    users.writeUTF(contraseña);
}

public boolean findUser(String usuario,String contraseña,String Genero,String correo,String cedula)throws IOException{
    users.seek(0);
    String user,password;
    while (users.getFilePointer() < users.length()) {
        user = users.readUTF();
        password = users.readUTF();
        if (user.equals(usuario) && password.equals(contraseña))
            return true;
        }
        return false;
    } 
    
    public void CrearArchivo() {
        try {
            File archivo = new File("Archi_Registro_Admi_Pasajeros.txt");
            archivo.createNewFile();

        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }

        public void InsertarEnArchivo(String lista) {
       int confirmado = JOptionPane.showConfirmDialog(null,"Esta seguro de la operacion?","Seleccione una opcion" , JOptionPane.YES_NO_OPTION);
            if (JOptionPane.OK_OPTION == confirmado) {     
        try {
            File archivo = new File("Archi_Registro_Admi_Pasajeros.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(lista + "\r\n");
            JOptionPane.showMessageDialog(null, "El archivo fue salvado correctamente!");
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "ERROR: Ocurrio un problema al salvar el archivo!" + e.getMessage());
            //System.out.println("Error al escribir en el archivo" + e);
        }
            }
    }
    
}
