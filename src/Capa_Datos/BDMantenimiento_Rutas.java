/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author Nikki
 */
public class BDMantenimiento_Rutas {
    
    public ArrayList<String> LeerDesdeArchivo() {
        ArrayList<String> contenido = new ArrayList<>();
        try {
            File archivo = new File("MantenimientoTerminal.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                contenido.add(archi.readLine());
            }
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al leer del archivo" + e);
        }
        return contenido;
    }
}
