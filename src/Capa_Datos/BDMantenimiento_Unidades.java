/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikki
 */
public class BDMantenimiento_Unidades {
    
    public void CrearArchivoUnidades(){
           try {
            File archivo = new File("MantenimientoUnidades.txt");
            archivo.createNewFile();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al crear el archivo",
                    "Mensaje de error", JOptionPane.ERROR_MESSAGE);
        }
    }
    public void InsertarEnArchivoUnidades(String lista) {
        try {
            File archivo = new File("MantenimientoUnides.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, false));
            archi.write(lista + "\r\n");
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al escribir en el archivo" + e);
        }
    }
    public ArrayList<String> LeerDesdeArchivoUnidades() {
        ArrayList<String> contenido = new ArrayList<>();
        try {
            File archivo = new File("MantenimientoUnidades.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                contenido.add(archi.readLine());
            }
            archi.close();
        } catch (Exception e) {
            System.out.println("Error al leer del archivo" + e);
        }
        return contenido;
    }
}
