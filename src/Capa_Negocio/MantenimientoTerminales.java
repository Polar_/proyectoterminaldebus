/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Negocio;

import Capa_Datos.BDMantenimiento_Terminal;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikki
 */

public class MantenimientoTerminales {
    BDMantenimiento_Terminal mante = new BDMantenimiento_Terminal();
    
    public void InsertarPersonas(String lista){
        mante.InsertarEnArchivo(lista);
        JOptionPane.showMessageDialog(null, "Usuario registrado exitosamente");
    }
    
   /* public ArrayList<String> LeerTerminal() {
        ArrayList<String> listaTerminal = mante.LeerDesdeArchivo();
        return listaTerminal;
    }*/
    
    public void InsertarNombreTerminal(String lista){
        mante.InsertarNombresTerminal(lista);
        JOptionPane.showMessageDialog(null, "Usuario registrado exitosamente");
    }
    
     public ArrayList<String> LeerNombreTerminal() {
        ArrayList<String> listaNombres = mante.LeerDesdeArchivoNombres();
        return listaNombres;
    }
}
