package Capa_Negocio;

import javax.swing.JOptionPane;

/**
 *
 * @author Nikki
 */
public class Seguridad {

    String res;

    public void validarUsuario(String usuarios[], String user, String pwd, int intentos) {
        boolean encontrado = false;

        for (int i = 0; i < usuarios.length; i++) {
            if ((usuarios[i].equalsIgnoreCase(user) && usuarios[i + 1].equals(pwd))) {
                res = "Bienvenido" + user;
                encontrado = true;
                JOptionPane.showMessageDialog(null, res, "Inicio de Sesión", JOptionPane.INFORMATION_MESSAGE);
            intentos = 0;
            break;
            }
        }// fin del para
        if (encontrado == false) {
            res = "Clave y/o usuario Erroneos van: "+ intentos+" intentos fallidos";
            JOptionPane.showMessageDialog(null, res, "Inicio de Sesión", JOptionPane.ERROR_MESSAGE);
            
        }
        if (intentos >2) {
            JOptionPane.showMessageDialog(null, "3 Intentos Fallidos, finalizando programa", "Inicio de Sesión", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

}
