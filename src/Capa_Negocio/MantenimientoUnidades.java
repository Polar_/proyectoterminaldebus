/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Capa_Negocio;


import Capa_Datos.BDMantenimiento_Unidades;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Nikki
 */
public class MantenimientoUnidades {
     BDMantenimiento_Unidades mante = new BDMantenimiento_Unidades();
    
    public void InsertarPersonas(String lista){
        mante.InsertarEnArchivoUnidades(lista);
        JOptionPane.showMessageDialog(null, "Datos Guardados con Exito!");
    }
    
    public ArrayList<String> LeerUnidades() {
        ArrayList<String> listaTerminal = mante.LeerDesdeArchivoUnidades();
        return listaTerminal;
    }
}
